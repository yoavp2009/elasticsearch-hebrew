FROM docker.elastic.co/elasticsearch/elasticsearch:7.10.2

COPY elasticsearch-analysis-hebrew-7.10.2.zip /usr/share/elasticsearch/elasticsearch-analysis-hebrew.zip

RUN /usr/share/elasticsearch/bin/elasticsearch-plugin install --batch file:///usr/share/elasticsearch/elasticsearch-analysis-hebrew.zip
RUN rm /usr/share/elasticsearch/elasticsearch-analysis-hebrew.zip
