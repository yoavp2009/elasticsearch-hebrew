#!/bin/bash

curl -X PUT -H 'Content-Type: application/json' \
   -d '{
      "index_patterns": [
        "cookbook-*"
      ],
      "priority": 0,
      "template": {
        "settings": {
          "number_of_shards": "1",
          "number_of_replicas": "0"
        },
        "mappings": {
          "dynamic_templates": [
            {
              "strings": {
                "match_mapping_type": "string",
                "mapping": {
                  "type": "text",
                  "analyzer": "hebrew",
                  "fields": {
                    "raw": {
                      "type": "keyword",
                      "ignore_above": 256
                    }
                  }
                }
              }
            }
          ],
          "properties": {
            "images_ids": {
              "type": "keyword"
            },
            "notes": {
              "analyzer": "hebrew",
              "type": "text"
            },
            "insertion_time": {
              "format": "dd/MM/yyyy HH:mm:ss",
              "type": "date"
            },
            "update_time": {
              "format": "dd/MM/yyyy HH:mm:ss",
              "type": "date"
            },
            "sender_name": {
              "analyzer": "hebrew",
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword",
                  "ignore_above": 256
                }
              }
            },
            "source": {
              "analyzer": "hebrew",
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword",
                  "ignore_above": 512
                }
              }
            },
            "title": {
              "analyzer": "hebrew",
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword",
                  "ignore_above": 512
                }
              }
            },
            "tags": {
              "analyzer": "hebrew",
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                }
              }
            },
            "times_prepared": {
              "format": "dd/MM/yyyy",
              "type": "date"
            },
            "grade": {
              "type": "short"
            },
            "yield": {
              "properties": {
                "measurement_unit": {
                  "type": "keyword"
                },
                "value": {
                  "type": "short"
                }
              }
            },
            "instructions_sections": {
              "type": "nested",
              "properties": {
                "instructions": {
                  "analyzer": "hebrew",
                  "type": "text"
                },
                "title": {
                  "analyzer": "hebrew",
                  "type": "text",
                  "fields": {
                    "raw": {
                      "type": "keyword",
                      "ignore_above": 256
                    }
                  }
                }
              }
            },
            "ingredients_sections": {
              "type": "nested",
              "properties": {
                "ingredients": {
                  "analyzer": "hebrew",
                  "type": "text"
                },
                "title": {
                  "analyzer": "hebrew",
                  "type": "text",
                  "fields": {
                    "raw": {
                      "type": "keyword",
                      "ignore_above": 256
                    }
                  }
                }
              }
            },
            "id": {
              "type": "keyword"
            }
          }
        }
      }
    }' \
http://localhost:9200/_index_template/cookbook
